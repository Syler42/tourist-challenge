﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TouristChallenge.Startup))]
namespace TouristChallenge
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
